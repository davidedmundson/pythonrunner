#!/bin/env python3
from gi.repository import GLib
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop

DBusGMainLoop(set_as_default=True)

objpath="/dave"

iface="org.kde.krunner1"
class Runner(dbus.service.Object):
    def __init__(self):
        dbus.service.Object.__init__(self, dbus.service.BusName("net.dave2", dbus.SessionBus()), objpath)

    @dbus.service.method(iface, out_signature='a(sss)')
    def Actions(self, msg):
        return []

    @dbus.service.method(iface, in_signature='s', out_signature='a(sssida{sv})')
    def Match(self, query):
        if query == "someString":
            #actionId, actionName, iconName, Type, relevance (0-1), properties
            return [("action1","Action Text","document-edit",100,0.9,{})]

    @dbus.service.method(iface, in_signature='ss',)
    def Run(self, matchId, actionId):
        if matchId == "action1":
            #do a thing here, like a webquery or something
            pass
        return

runner = Runner()
loop = GLib.MainLoop()
loop.run()
